package net.portic.conlog;

import java.io.IOException;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TestIntervalMapper2 extends Mapper<LongWritable, Text, Text, Text> {

	public String low;
	public String high;
	public String level;
	private final static Text one = new Text("");


	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapreduce.Mapper#setup(org.apache.hadoop.mapreduce.Mapper.Context)
	 */
	protected void setup(Context context) throws IOException, InterruptedException {
		Configuration conf = context.getConfiguration();
		low = conf.get("low");
		high = conf.get("high");
		level = conf.get("level");
	}
	
	/**
	 * @param line
	 * @return
	 */
	private String[] parseLine(String line) {
		String[] res = null;
		String pattern = "\\s*(\\d*)-(\\d*)-(\\d*) (\\d*):(\\d*):(\\d*),(\\d*) \\S* (\\S*)  - (...*)";

		try {
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(line);
			if (m.find()) {
				res = new String[m.groupCount() + 1];
				for (int i = 0; i < m.groupCount() + 1; i++) {
					res[i] = m.group(i);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;

	}
	
	/**
	 * @param currentLevel
	 * @return
	 */
	private boolean levelOk(String currentLevel) {
		return currentLevel.equals(level);
	}


	@Override
	public void map(LongWritable _key, Text value, Context context)
		throws IOException, InterruptedException {

		String TempString = value.toString();
		String[] s = parseLine(TempString);
		
		/*
		 * 
		 * 1. 2013 2. 11 3. 21 4. 15 5. 59 6. 30 7. 991 8. INFO 9.
		 * mapred.output.value.class is deprecated. Instead, use
		 * mapreduce.job.output.value.class
		 */
		
		if (s != null && s.length == 10) {
			Calendar cLow = Calendar.getInstance();
			Calendar cHigh = Calendar.getInstance();
			Calendar date = Calendar.getInstance();
			date.set(Integer.parseInt(s[1]), Integer.parseInt(s[2]),
					Integer.parseInt(s[3]), Integer.parseInt(s[4]),
					Integer.parseInt(s[5]), Integer.parseInt(s[6]));
			cLow.set(Integer.parseInt(low.substring(0, 4)), Integer.parseInt(low.substring(4, 6)), Integer.parseInt(low.substring(6, 8)), Integer.parseInt(low.substring(8, 10)), Integer.parseInt(low.substring(10, 12)), Integer.parseInt(low.substring(12, 14)));
			cHigh.set(Integer.parseInt(high.substring(0, 4)), Integer.parseInt(high.substring(4, 6)), Integer.parseInt(high.substring(6, 8)), Integer.parseInt(high.substring(8, 10)), Integer.parseInt(high.substring(10, 12)), Integer.parseInt(high.substring(12, 14)));
			if (date.compareTo(cHigh) < 0 && date.compareTo(cLow) > 0 && levelOk(s[8])) {
				//output.collect(new Text(TempString), one);
				context.write(new Text(TempString), one);
			}
		}
	}
}
