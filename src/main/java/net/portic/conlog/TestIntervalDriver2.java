package net.portic.conlog;

//imported jars
import java.io.File;
import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class TestIntervalDriver2 {
	public static void main(String[] args) throws Exception {
		
		if (args.length < 5) {
			System.out.println("Falten parametres per a executar aquest job");
			return;
		}
		
		Job job = new Job();
		
		// Necessari per ClassNotFound
		job.setJarByClass(TestIntervalDriver2.class);
		
		// Name of the Job
		job.setJobName("TestInterval1.0");
		job.getConfiguration().set("low", args[2]);
		job.getConfiguration().set("high", args[3]);
		job.getConfiguration().set("level", args[4]);

		// Data type of Output Key and Value
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		// Setting the Mapper and Reducer Class
		job.setMapperClass(TestIntervalMapper2.class);
		job.setReducerClass(TestIntervalReducer2.class);

		// Formats of the Data Type of Input and output
		//conf.setInputFormat(TextInputFormat.class);
		//conf.setOutputFormat(TextOutputFormat.class);

		// Specify input and output DIRECTORIES (not files)
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		
		cleanEnv(args);
		
		System.exit(job.waitForCompletion(true)?0:1);
	}

	public static void cleanEnv(String[] args) {
		File directory = new File(args[1]);
		// make sure directory exists
		if (!directory.exists()) {
			System.out.println("Directory does not exist.");
		} else {
			try {
				delete(directory);
				System.out.println("Directory "+args[1]+" has been deleted.");
				
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		} // Running the job with Configurations set in the conf.

	}

	public static void delete(File file) throws IOException {

		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				file.delete();
				System.out.println("Directory is deleted : "
						+ file.getAbsolutePath());

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					delete(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
					System.out.println("Directory is deleted : "
							+ file.getAbsolutePath());
				}
			}

		} else {
			// if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}
}
