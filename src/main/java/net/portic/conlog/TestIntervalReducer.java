package net.portic.conlog;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class TestIntervalReducer extends MapReduceBase implements
		org.apache.hadoop.mapred.Reducer<Text, Text, Text, Text> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.mapred.Reducer#reduce(java.lang.Object,
	 * java.util.Iterator, org.apache.hadoop.mapred.OutputCollector,
	 * org.apache.hadoop.mapred.Reporter)
	 */
	@Override
	public void reduce(Text _key, Iterator<Text> values,
			OutputCollector<Text, Text> output, Reporter reporter)
			throws IOException {
		Text key = _key;
		StringBuffer line = new StringBuffer();
		while (values.hasNext()) {
			// replace ValueType with the real type of your value
			Text value = values.next();
			line.append(value.toString());
			// process value
		}
		output.collect(key, new Text(line.toString()));
	}
}
