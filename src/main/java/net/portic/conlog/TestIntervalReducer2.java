package net.portic.conlog;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.Reducer;

public class TestIntervalReducer2 extends Reducer<Text, Text, Text, Text> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.mapred.Reducer#reduce(java.lang.Object,
	 * java.util.Iterator, org.apache.hadoop.mapred.OutputCollector,
	 * org.apache.hadoop.mapred.Reporter)
	 */
	@Override
	public void reduce(Text _key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		Text key = _key;
		StringBuffer line = new StringBuffer();
		for (Text value : values) {
			// replace ValueType with the real type of your value
			line.append(value.toString());
			// process value
		}
		context.write(key, new Text(line.toString()));
	}
}
