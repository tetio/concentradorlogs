package net.portic.conlog;

//imported jars
import java.io.File;
import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

public class TestIntervalDriver {
	public static void main(String[] args) {
		
		if (args.length < 4) {
			System.out.println("Falten parametres per a executar aquest job");
			return;
		}
		
		JobClient client = new JobClient();
		// Configurations for Job set in this variable
		JobConf conf = new JobConf(net.portic.conlog.TestIntervalDriver.class);

		// Name of the Job
		conf.setJobName("TestInterval1.0");
		conf.set("low", args[2]);
		conf.set("high", args[3]);

		// Data type of Output Key and Value
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		// Setting the Mapper and Reducer Class
		conf.setMapperClass(net.portic.conlog.TestIntervalMapper.class);
		conf.setReducerClass(net.portic.conlog.TestIntervalReducer.class);

		// Formats of the Data Type of Input and output
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		// Specify input and output DIRECTORIES (not files)
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		client.setConf(conf);
		try {
			cleanEnv(args);
			JobClient.runJob(conf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void cleanEnv(String[] args) {
		File directory = new File(args[1]);
		// make sure directory exists
		if (!directory.exists()) {
			System.out.println("Directory does not exist.");
		} else {
			try {
				delete(directory);
				System.out.println("Directory "+args[1]+" has been deleted.");
				
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		} // Running the job with Configurations set in the conf.

	}

	public static void delete(File file) throws IOException {

		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				file.delete();
				System.out.println("Directory is deleted : "
						+ file.getAbsolutePath());

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					delete(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
					System.out.println("Directory is deleted : "
							+ file.getAbsolutePath());
				}
			}

		} else {
			// if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}
}
