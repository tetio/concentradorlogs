package net.portic.conlog;

import java.io.IOException;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.configuration.MapConfiguration;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.JobConfigurable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.Mapper.Context;

public class TestIntervalMapper extends MapReduceBase implements
		org.apache.hadoop.mapred.Mapper<LongWritable, Text, Text, Text> {

	public String low;
	public String high;
	private final static Text one = new Text("");

	protected void setup(Context context) throws IOException, InterruptedException {
		System.out.println("setup 0");
		Configuration conf = context.getConfiguration();
		low = conf.get("low");
		high = conf.get("high");
		System.out.println("setup!");
	}
	
	private String[] parseLine(String line) {
		String[] res = null;
		String pattern = "\\s*(\\d*)-(\\d*)-(\\d*) (\\d*):(\\d*):(\\d*),(\\d*) \\S* (\\S*)  - (...*)";

		try {
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(line);
			if (m.find()) {
				res = new String[m.groupCount() + 1];
				for (int i = 0; i < m.groupCount() + 1; i++) {
					res[i] = m.group(i);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.hadoop.mapred.Mapper#map(java.lang.Object,
	 * java.lang.Object, org.apache.hadoop.mapred.OutputCollector,
	 * org.apache.hadoop.mapred.Reporter)
	 */
	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapred.Mapper#map(java.lang.Object, java.lang.Object, org.apache.hadoop.mapred.OutputCollector, org.apache.hadoop.mapred.Reporter)
	 */
	@Override
	public void map(LongWritable _key, Text value,
			OutputCollector<Text, Text> output, Reporter reporter)
		throws IOException {

		String TempString = value.toString();
		String[] s = parseLine(TempString);
		
		/*
		 * 
		 * 1. 2013 2. 11 3. 21 4. 15 5. 59 6. 30 7. 991 8. INFO 9.
		 * mapred.output.value.class is deprecated. Instead, use
		 * mapreduce.job.output.value.class
		 */
		
		if (s != null && s.length == 10) {
			Calendar cLow = Calendar.getInstance();
			Calendar cHigh = Calendar.getInstance();
			Calendar date = Calendar.getInstance();
			date.set(Integer.parseInt(s[1]), Integer.parseInt(s[2]),
					Integer.parseInt(s[3]), Integer.parseInt(s[4]),
					Integer.parseInt(s[5]), Integer.parseInt(s[6]));
			cLow.set(Integer.parseInt(low.substring(0, 4)), Integer.parseInt(low.substring(4, 6)), Integer.parseInt(low.substring(6, 8)), Integer.parseInt(low.substring(8, 10)), Integer.parseInt(low.substring(10, 12)), Integer.parseInt(low.substring(12, 14)));
			cHigh.set(Integer.parseInt(high.substring(0, 4)), Integer.parseInt(high.substring(4, 6)), Integer.parseInt(high.substring(6, 8)), Integer.parseInt(high.substring(8, 10)), Integer.parseInt(high.substring(10, 12)), Integer.parseInt(high.substring(12, 14)));
			if (date.compareTo(cHigh) < 0 && date.compareTo(cLow) > 0) {
				output.collect(new Text(TempString), one);
			}
		}
	}
}
